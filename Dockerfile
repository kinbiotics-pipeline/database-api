FROM ubuntu:latest
RUN apt-get update -y && apt-get install -y python3-pip apache2 apache2-dev 
COPY ./src /app
WORKDIR /app
RUN pip install -r /app/requirements.txt
ENTRYPOINT ["mod_wsgi-express", "start-server", "--user=www-data", "wsgi.py"]
