# KINBIOTICS Database with Flask API

This repository provides the definition of the KINBIOTICS database to store the workflow results and a simple REST-API to access it. 

## Setup and run

The setup is completely contained in [Docker](https://docker.io) and only Docker with the compose Plugin is needed to run it. On some systems also docker-compse needs to be installed separately.

Before starting the service for the fist time, a database user and password must be entered in the `docker-compose.yaml`. Both can be choosen freely, but the password should be rather strong, since it never has to be typed in manually afterwards and it protects the database.

Afterwards, with docker running, just execute

```
docker-compose up
```

in the cloned repository, optionally with `-d` to start the process in the backgroud.

## Updating files

After changing a file (e.g. `config.py`) the containers can be updated and restarted with the command

```
docker-compose up -d --build --force-recreate
```
