# noinspection PyInterpreter
import os


class Config:
    # The possibility to pass these values through the environment enables the use with docker secrets
    # get database password from file, if it is specified, otherwise from env or directly from this config in this order
    pwfile = os.environ.get("DB_PASSWORD_FILE")
    if pwfile:
        with open(pwfile) as f:
            DB_PASSWORD = f.readline().rstrip("\n")
    else:
        DB_PASSWORD = os.environ.get('DB_PASSWORD') or ''

    # get database user from file, if it is specified, otherwise from env or directly from this config in this order
    userfile = os.environ.get('DB_USER_FILE')
    if userfile:
        with open(userfile) as f:
            DB_USER = f.readline().rstrip("\n")
    else:
        DB_USER = os.environ.get('DB_USER') or 'kinbiotics'

    DB_HOST = os.environ.get('DB_HOST') or '127.0.0.1'
    DB_NAME = os.environ.get('DB_NAME') or 'kinbiotics'

    ## Database table definitions

    SPECIES_TABLE_NAME = 'species_edge'
    SPECIES_TABLE_COLUMNS = 'species_accession text, query_id text, query_length integer, query_start integer,' \
                            'query_end integer, strand char,' \
                            'target_length integer, target_start integer, target_end integer,' \
                            'residue_matches integer, alignment_block_length integer, mapping_quality integer,' \
                            'match_percentage real, species_name text, match_percentage_bin text, score real, source text, count integer'

    ABR_TABLE_NAME = 'abr_edge'
    ABR_TABLE_COLUMNS = 'abr_card_id text, query_id text, query_length integer, query_start integer,' \
                        'query_end integer, strand char,' \
                        'target_length integer, target_start integer, target_end integer, residue_matches integer,' \
                        'alignment_block_length integer, mapping_quality integer,' \
                        'match_percentage real, match_percentage_bin text, score real, source text, count integer'

    RGI_TABLE_NAME = 'rgi'
    RGI_TABLE_COLUMNS = 'hit_seq_id text, hit_id text, type_match text, orf_strand char, orf_start int, orf_end int,' \
                        'orf_from text, model_name text, model_type text, model_type_id int, model_id int,' \
                        'pass_evalue text, pass_bitscore real, ARO_accession text, ARO_name text, evalue numeric,' \
                        'bit_score real, max_identities int, cvterm_id int, partial int, query_start int,' \
                        'query_end int, perc_identity real, source text, note text, nudged text, missed_bases_by_prodigal text,' \
                        'orf_prot_sequence_possible text, orf_start_possible int, orf_end_possible int, orf_dna_sequence_possible text'

    GTDBTK_TABLE_NAME = 'gtdb'
    GTDBTK_TABLE_COLUMNS = 'classification text, classification_method text, closest_placement_af real,' \
                           'closest_placement_ani real, closest_placement_radius real, closest_placement_reference text,' \
                           'closest_placement_taxonomy text, fastani_af real, fastani_ani real, fastani_reference text,' \
                           'fastani_reference_radius real, fastani_taxonomy text, msa_percent real, note text,' \
                           'other_related_references text, pplacer_taxonomy text,' \
                           'red_value text, source text, translation_table text, user_genome text, warnings text'

    INVALID_SCHEMA_CHARS = '[^A-Z,a-z,0-9]+'
    SCHEMA_REPLACEMENT_CHAR = '_'
