import json

from flask import Flask, request, abort
import psycopg2
import psycopg2.extras
import re
from config import Config

app = Flask(__name__)

def sanitize_schema_name(name):

    r = re.compile(Config.INVALID_SCHEMA_CHARS)

    subs_name = re.sub(r, Config.SCHEMA_REPLACEMENT_CHAR, name)

    if subs_name.startswith((Config.SCHEMA_REPLACEMENT_CHAR, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9')):
        return f"X{subs_name}"
    else:
        return subs_name


def db_connect():
    """
    Establish a connection to the database.

    :return: Database connection object
    """
    con = None
    try:
        con = psycopg2.connect(host=Config.DB_HOST, database=Config.DB_NAME,
                               user=Config.DB_USER, password=Config.DB_PASSWORD)
    except psycopg2.OperationalError:
        abort(500, f"Connection to Database {Config.DB_NAME} failed!" )
    return con


@app.get('/<project>')
def get_project(project):
    """
    Fetch all information for given project.

    :param project: Name of the project/analysis
    :return: JSON list of detected species and abr
    """
    project = sanitize_schema_name(project)

    species = get_species(project)
    abr = get_abr(project)
    gtdb = get_gtdb(project)
    rgi = get_rgi(project)

    return {"species": species, "abr": abr, "gtdb": gtdb, "rgi": rgi}

# Edge workflow endpoints

@app.get('/<project>/species')
def get_species(project):
    """
    Fetch all detected species for given project.

    :param project: Name of the project/analysis
    :return: JSON list of detected species
    """
    project = sanitize_schema_name(project)

    with db_connect().cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cur:
        cur.execute(f"SELECT * FROM {project}.{Config.SPECIES_TABLE_NAME};")
        res = cur.fetchall()

    return res


@app.get('/<project>/abr')
def get_abr(project):
    """
    Fetch all detected antibiotic resistances for given project.

    :param project: Name of the project/analysis
    :return: JSON list of detected ABRs.
    """
    project = sanitize_schema_name(project)

    with db_connect().cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cur:
        cur.execute(f"SELECT * FROM {project}.{Config.ABR_TABLE_NAME};")
        res = cur.fetchall()

    return res


@app.post('/<project>/species')
def insert_species(project):
    """
    Read species data as JSON and store it in the database.

    :param project: Name of the project/analysis
    :return: nothing / HTTP status 200
    """
    project = sanitize_schema_name(project)

    species = json.loads(request.data)
    db_connection = db_connect()

    with db_connection.cursor() as cur:
        # Create a new schema for each analysis
        cur.execute(f"CREATE SCHEMA IF NOT EXISTS {project};")
        # Create the species table, if it not already exists.
        cur.execute(f"CREATE TABLE IF NOT EXISTS {project}.{Config.SPECIES_TABLE_NAME} ({Config.SPECIES_TABLE_COLUMNS});")
        for spec in species:
            # Insert the detected species.
            data = {k: ('' if v is None else v) for k, v in spec.items()}  # convert None values to empty strings
            cols = ', '.join(data.keys())
            vals = [val for val in data.values()]
            valplaceholder = ', '.join(['%s'] * len(data.keys()))

            cur.execute(f"INSERT INTO {project}.{Config.SPECIES_TABLE_NAME} ({cols}) "
                        f"VALUES ({valplaceholder}); ",
                        vals)
        try:
            db_connection.commit()

        except KeyError as e:
            return f'Error, unsupported key in payload: {e}', 501

    return '', 200


@app.post('/<project>/abr')
def insert_abr(project):
    """
    Read antibiotic resistance (ABR) data as JSON and store it in the database.

    :param project: Name of the project/analysis
    :return: nothing / HTTP status 200
    """
    project = sanitize_schema_name(project)

    abr = json.loads(request.data)
    db_connection = db_connect()

    with db_connection.cursor() as cur:
        # Create a new schema for each analysis
        cur.execute(f"CREATE SCHEMA IF NOT EXISTS {project};")
        # Create the ABR table, if it not already exists.
        cur.execute(f"CREATE TABLE IF NOT EXISTS {project}.{Config.ABR_TABLE_NAME} ({Config.ABR_TABLE_COLUMNS});")
        for ab in abr:
            # Insert the detected ABR.
            data = {k: ('' if v is None else v) for k, v in ab.items()}  # convert None values to empty strings
            cols = ', '.join(data.keys())
            vals = [val for val in data.values()]
            valplaceholder = ', '.join(['%s'] * len(data.keys()))

            cur.execute(f"INSERT INTO {project}.{Config.ABR_TABLE_NAME} ({cols}) "
                        f"VALUES ({valplaceholder}); ",
                        vals)
        try:
            db_connection.commit()
        except KeyError as e:
            return f'Error, unsupported key in payload: {e}', 501

        db_connection.commit()

    return '', 200

# Cloud workflow endpoints

@app.get('/<project>/rgi')
def get_rgi(project):
    """
    Fetch all rgi results for the given project.

    :param project: Name of the project/analysis
    :return: JSON list of detected ABRs.
    """
    project = sanitize_schema_name(project)

    with db_connect().cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cur:
        cur.execute(f"SELECT * FROM {project}.{Config.RGI_TABLE_NAME};")
        res = cur.fetchall()

    return res


@app.get('/<project>/gtdb')
def get_gtdb(project):
    """
    Fetch all GTDB-tk results for the given project.

    :param project: Name of the project/analysis
    :return: JSON list of detected ABRs.
    """
    project = sanitize_schema_name(project)

    with db_connect().cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cur:
        cur.execute(f"SELECT * FROM {project}.{Config.GTDBTK_TABLE_NAME};")
        res = cur.fetchall()

    return res

@app.post('/<project>/rgi')
def insert_rgi(project):
    """
    Read rgi result data as JSON and store it in the database.

    :param project: Name of the project/analysis
    :return: nothing / HTTP status 200
    """
    project = sanitize_schema_name(project)

    rgi = json.loads(request.data)
    db_connection = db_connect()

    with db_connection.cursor() as cur:
        # Create a new schema for each analysis
        cur.execute(f"CREATE SCHEMA IF NOT EXISTS {project};")
        # Create the RGI table, if it not already exists.
        cur.execute(f"CREATE TABLE IF NOT EXISTS {project}.{Config.RGI_TABLE_NAME} ({Config.RGI_TABLE_COLUMNS});")
        for r in rgi:
            """
            Insert the RGI results
            """
            data = {k: ('' if v is None else v) for k, v in r.items()}  # convert None values to empty strings
            cols = ', '.join(data.keys())
            vals = [val for val in data.values()]
            valplaceholder = ', '.join(['%s'] * len(data.keys()))

            cur.execute(f"INSERT INTO {project}.{Config.RGI_TABLE_NAME} ({cols}) "
                        f"VALUES ({valplaceholder}); ",
                        vals)
        try:
            db_connection.commit()
        except KeyError as e:
            return f'Error, unsupported key in payload: {e}', 501

        db_connection.commit()

    return '', 200


@app.post('/<project>/gtdb')
def insert_gtdb(project):
    """
    Read GTDB-tk result data as JSON and store it in the database.

    :param project: Name of the project/analysis
    :return: nothing / HTTP status 200
    """
    project = sanitize_schema_name(project)

    gtdb = json.loads(request.data)
    db_connection = db_connect()

    with db_connection.cursor() as cur:
        # Create a new schema for each analysis
        cur.execute(f"CREATE SCHEMA IF NOT EXISTS {project};")
        # Create the GTDB table, if it not already exists.
        cur.execute(f"CREATE TABLE IF NOT EXISTS {project}.{Config.GTDBTK_TABLE_NAME} ({Config.GTDBTK_TABLE_COLUMNS});")
        for gt in gtdb:
            """
            Insert the GTDB-Tk results.
            """
            data = {k:  v for k, v in gt.items()}
            cols = ', '.join(data.keys())
            vals = [val for val in data.values()]
            valplaceholder = ', '.join(['%s'] * len(data.keys()))

            cur.execute(f"INSERT INTO {project}.{Config.GTDBTK_TABLE_NAME} ({cols}) "
                        f"VALUES ({valplaceholder}); ",
                        vals)
        try:
            db_connection.commit()
        except KeyError as e:
            return f'Error, unsupported key in payload: {e}', 501

        db_connection.commit()

    return '', 200


if __name__ == '__main__':
    app.run()
